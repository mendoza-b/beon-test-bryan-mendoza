import { Box, Typography, Avatar } from '@mui/material';
import type { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import type { IContact } from 'react-coding-interview-shared/models';
import React, { useState } from 'react';

const SCHEMAS = {
  text: (value: unknown): value is string => {
    return typeof value === 'string';
  },
  email: (value: string) => {
    if (!value.includes('@')) {
      throw new Error('Invalid email address');
    }
  },
};

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [willAllowEdit, setWillAllowEdit] = useState(false);

  const handleDoubleClick = () => {
    setWillAllowEdit(true);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = new FormData(e.currentTarget);

    const formEntries = Object.fromEntries(formData.entries());

    const name = formEntries['name'];

    if (!SCHEMAS.text(name)) {
      //  do something

      return;
    }

    try {
      const email = SCHEMAS.email(name);

      // make api call to update user
    } catch (error) {
      console.log('Invalid email address');

      return {
        // email: 'Invalid email address',
        email: ['Invalid email address'],
      };
    }
  };

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />

        <Box textAlign="center" mt={2}>
          <form onSubmit={handleSubmit}>
            <EditField
              renderReadonlyField={(name) => (
                <Typography variant="caption" color="text.secondary">
                  {name}
                </Typography>
              )}
              text={name}
              willAllowEdit={willAllowEdit}
              onDoubleClick={handleDoubleClick}
              fieldName="name"
            />
          </form>

          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};

type EditFieldProps = {
  renderReadonlyField: (text: string) => React.ReactNode;
  text: string;
  willAllowEdit: boolean;
  fieldName: string;
  onDoubleClick?: React.MouseEventHandler<HTMLDivElement>;
};

const EditField = (props: EditFieldProps) => {
  const {
    renderReadonlyField,
    text,
    willAllowEdit,
    onDoubleClick,
    onInputChange,
    fieldName,
  } = props;

  return (
    <div onDoubleClick={onDoubleClick}>
      {willAllowEdit ? (
        <input style={{ display: 'block' }} value={text} name={fieldName} />
      ) : (
        renderReadonlyField(text)
      )}
    </div>
  );
};
