import { useMemo } from 'react';
import { useMatches } from 'react-router-dom';

import type { IRouteInfo } from '@lib/routes';
import { routeDefinitions } from '@lib/routes';

export function useRouteInfo() {
  const routeMatches = useMatches();

  const routeDefMap = useMemo<Record<string, IRouteInfo>>(() => {
    return Object.keys(routeDefinitions).reduce((acc, curr) => {
      return {
        ...acc,
        [curr]: routeDefinitions[curr],
      };
    }, {});
  }, []);

  const activeRouteData = useMemo(() => {
    for (let i = routeMatches.length - 1; i >= 0; i--) {
      const routeMapDefId = routeMatches[i]?.id;

      if (typeof routeMapDefId === 'string' && routeMapDefId in routeDefMap) {
        return routeDefMap[routeMapDefId];
      }
    }
  }, [routeMatches]);

  return activeRouteData || ({} as IRouteInfo);
}
